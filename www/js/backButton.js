// Back Button Handler
window.addEventListener("load", function() {
    window.addEventListener("popstate", function() {
        if (window.history.state !== "backhandler") {
            window.history.pushState("backhandler", null, null);
            BackHandler();

        }
    });
    window.history.pushState("backhandler", null, null);
});

function BackHandler() {
    var selectedCat = $('#selectedCat').css("display");
    var details_Wrap = $('.details_Wrap').css("display");
    var popupPage = $('#popupPage').css("display");
    var contentWrapList = $('#contentWrapList').css("display");

    if (popupPage == 'block') {
        screen.lockOrientation('portrait');
       closePage();

    } else if (details_Wrap == 'block') {
        $(".details_Wrap").hide();
        if(pageFrom=='search')
        {
        setTimeout(function(){ $("#popupPage").fadeIn(700);},200);
        }
        else
        {
         // $("#selectedCat").fadeIn(1000);
         if(contentWrapList=='block')
        {
            $("#contentWrapList").hide();
        setTimeout(function(){ $("#contentWrapList").fadeIn(700);},200);
        }
        else
        {
            $(".contentWrap").hide();
        setTimeout(function(){ $(".contentWrap").fadeIn(700);},200);
        }
/*      */
        
        }
         $(".header_com").show(); 
        // pageFrom =''; 
		
    } else if (contentWrapList == 'block') {
        $("#contentWrapList").hide();
        $("#selectedCat").html('');
        $("#carousalWrap").fadeIn(700);
    } else {
        //window.open("", "_self").close();
    }
}
